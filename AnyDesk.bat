@echo off
Title AnyDeskRemote
Mode 50,8
SETLOCAL EnableDelayedExpansion


For /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo 	on & for %%b in (1) do rem"') do (
  Set "DEL=%%a"
)
Color a1
Goto :input
Cls
:input
::echo x=msgbox("AnyDesk offline",0+48,"Move along!") > Error.vbs
::".\Error.vbs"
::del /f .\Error.vbs"
ping 8.8.8.8 -n 1 -w 5000 >nul
if errorlevel 1 goto anydeskoffline
Del /f PathAnyDesk.txt  :: delete file 

set build=
Where /r "C:\Program Files (x86)" AnyDes*.exe > PathAnyDesk.txt
Set /p Build=<PathAnyDesk.txt
if exist "%build%" (goto continue) else (goto NoAnyDesk)

:anydeskoffline
cls
Mode 60,5&color 0c
echo.&echo No connection.&echo Check the communication connection on this computer.
pause
goto exit
:continue
Color a1
Cls
For /f "delims=" %%i in ('"%build%" --get-status') do set status=%%i 
Echo 	AnyDesk Status is: %status%
For /f "delims=" %%i in ('"%build%" --get-id') do set ID=%%i 
Echo 	AnyDesk ID is: %ID%
Del /f PathAnyDesk.txt
Echo.&Echo 	Enter anydesk ID to connect:
Set /p id=AnyDesk ID:
Goto :connect
:connect
where /r "C:\Program Files (x86)" AnyDes*.exe > PathAnyDesk.txt 
Set /p Build=<PathAnyDesk.txt
Start "" /B "%build%" "%id%"
Del /f pathanydesk.txt
Goto :input

:NoAnyDesk
Color a1
Cls
call :ColorText 4e 	"AnyDesk is not installed on this computer."
echo.&Goto installanydesk
call function01
:installanydesk
echo.&echo To download and install Enter Y
Set /p YN=You want to install AnyDesk [Y/N]?: 
for %%v in (Y,y,N,n) do (
IF "%YN%"=="Y" goto yesinstallanydesk
IF "%YN%"=="y" goto yesinstallanydesk
IF "%YN%"=="N" goto noinstallanydesk
IF "%YN%"=="n" goto noinstallanydesk
IF "%YN%"=="e" goto exit
)
color c
echo Wrong choice.
ping localhost -n 2 >nul
goto :NoAnyDesk
:yesinstallanydesk
start https://www.460.co.il/upload/PsagotSupport.exe
move %userprofile%\Downloads\psagotsuppor*.exe %userprofile%\Desktop
start https://anydesk.com/en/downloads/windows?dv=win_exe
del /f PathAnyDesk.txt
Where /r "%userprofile%" AnyDes*.exe > PathAnyDesk.txt
Set /p pathfile=<PathAnyDesk.txt
%pathfile%  --install  "C:\Program Files (x86)\AnyDesk" --start-with-win --create-desktop-icon
del /f PathAnyDesk.txt
goto input

:noinstallanydesk
pause
exit
:exit
exit

:function01
#color
:ColorText
Echo off
<nul set /p ".=%DEL%" > "%~2"
findstr /v /a:%1 /R "^$" "%~2" nul
del "%~2" > nul 2>&1
goto :eof
#color
EXIT /B 0